using XLSX, DataFrames, StatsBase, RCall

# read FDA library xlsx, first sheet, all cells
fda_matrix = XLSX.readxlsx("drug_screen.xlsx")["approved_library"][:]

# read epigenetic library xlsx
epi_matrix = XLSX.readxlsx("drug_screen.xlsx")["epigenetic_library"][:]

# convert to DataFrame
fda_df = DataFrame(fda_matrix[2:end, :], Symbol.(fda_matrix[1, :]))
epi_df = DataFrame(epi_matrix[2:end, :], Symbol.(epi_matrix[1, :])) 

# fix typos in pathways, shorten etc
replace!(fda_df.Pathway, 
         missing => "Others",
         "others" => "Others",
         "Endocrinology&Hormones" => "Endocrinology",
         "Endocrinology & Hormones" => "Endocrinology",
         "TGF-beta/Smad" => "TGF-β/Smad",
         "Stem Cells &  Wnt" => "Stemness\n& Wnt")

replace!(epi_df.Pathway,
         missing => "Others",
         "Membrane Transporter/Ion Channel" => "Ion\nchannel",
         "Immunology/Inflammation" => "Immunity",
         "PI3K/Akt/mTOR Signaling" => "PI3K/Akt/mTOR",
         "Ubiquitination/Proteasome" => "Ubiquination\n/ Proteasome",
         "MAPK Signaling" => "MAPK",
         "GPCR/G protein" => "G protein",
         "Cell Cycle/Checkpoint" => "Cell Cycle",
         "Epigenetics" => "Chromatin/Epigenetics")

# count instances of each pathway
fda_countmap = countmap(fda_df.Pathway)
fda_pathcounts = DataFrame(pathway = collect(keys(fda_countmap)),
                           n = collect(values(fda_countmap)))

epi_countmap = countmap(epi_df.Pathway)
epi_pathcounts = DataFrame(pathway = collect(keys(epi_countmap)),
                           n = collect(values(epi_countmap)))

# don't show unclassified drugs
fda_knownpathways = filter(x -> x.pathway != "Others", fda_pathcounts)
epi_knownpathways = filter(x -> x.pathway != "Others", epi_pathcounts)

# call R to visualise pathways + targets
R"""
library(ggplot2); library(packcircles); library(cowplot); library(Cairo)
circles <- function(data) { 
    packing <- circleProgressiveLayout(data$n, sizetype='area')
    data <- cbind(data, packing)
    dat.gg <- circleLayoutVertices(packing, npoints=50)

      plot <- ggplot() + 
      geom_polygon(data = dat.gg, 
                   aes(x, y, 
                       group = id, 
                       fill=as.factor(id)), 
                       colour = "black", 
                       alpha = 0.6) +
      geom_text(data = data, 
                aes(x, y, 
                    label = pathway, 
                    family = "FreeSans", 
                    fontface = "bold")) +
      scale_size_continuous(range = c(1,4)) +
      theme_void() + 
      theme(legend.position="none",
            plot.margin = unit(c(0,0,0,0),"cm")) +
      coord_equal()
      plot
}
both <- plot_grid(circles($fda_knownpathways),
                  circles($epi_knownpathways),
                  ncol=1,
                  labels = c('Approved', 'Epigenetic'))

ggsave("images/library_pathways.svg", 
       both,
       device = CairoSVG,
       width = 20, height = 25)
"""






