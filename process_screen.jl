const USAGE = """
Usage: 
julia process_screen.jl path/data.xlsx path/output/dir
 data: XLSX file with screen data, layouts and library information
 outdir: directory to write results to
"""

if !isinteractive() && length(ARGS) != 2
    print(USAGE)
    exit()
end

using XLSX, CSV, DataFrames, RCall, Chain, StatsBase
using Statistics: mean, std, median

struct Screen
    cells::String
    libname::String
    plates::Array{Array{Int64, 2}, 1}
end

struct Library
    libname::String
    layout::Array{Array{String, 2}, 1}
    library::DataFrame
end

const COLNAMES = Dict("Catalog Number" => "drugid",
                      "CatalogNumber" => "drugid",
                      "Product Name" => "drugname",
                      "Item Name" => "drugname",
                      "CAS Number" => "CASnumber",
                      "M.w." => "molweight",
                      "Target" => "target",
                      "Pathway" => "pathway",
                      "Formula" => "formula")

const KEEPCOLS = [:drugid, :drugname, :target, :pathway, :molweight, :formula, :SMILES, :CASnumber]

const CELLS = Dict("raji" => "Raji",
                   "dg75" => "DG-75",
                   "bclq3" => "LCBQ533",
                   "del" => "DEL",
                   "mangosteen" => "Mangosteen",
                   "matoke" => "Matoke",
                   "sudhl" => "SU-DHL-1",
                   "fepd" => "FE-PD")

const SCREENS = Dict("approved" => "Regulator-approved drug library",
                     "epigenetic" => "Epigenetic drug library")

const PLATEORDER = repeat(1:5, inner = 3)
const REPORDER = repeat(1:3, 5)
                   
const PLATEWIDTH = 24 # 384-well plates
const PLATEHEIGHT = 16

function cleancol(old)
        if old ∈ keys(COLNAMES)
            COLNAMES[old]
        else
            old
        end
end

function cleantable(df::DataFrame)::DataFrame
    rename!(cleancol, df)
    select!(df, KEEPCOLS)
    # fix typos in pathways
    replace!(df.pathway, 
             missing => "Others",
             "others" => "Others",
             "Endocrinology&Hormones" => "Endocrinology",
             "Endocrinology & Hormones" => "Endocrinology",
             "TGF-beta/Smad" => "TGF-β/Smad",
             "Stem Cells &  Wnt" => "Stemness\n& Wnt")
    # strip whitespace from drug names
    df.drugname = strip.(df.drugname)
    df
end

"return (row, column) coordinates from column-major index"
function getcoords(i)
    i = i - 1
    col = div(i, PLATEHEIGHT)
    row = i - (col * PLATEHEIGHT)
    (row + 1, col + 1)
end

"return the manhattan distance from the plate edge"
function edgedistancemanhattan(index)
    (row, col) = getcoords(index)

    rowdist = min(row - 1, PLATEHEIGHT - row)
    coldist = min(col - 1, PLATEWIDTH - col)

    min(rowdist, coldist)
end

"scan stacked arrays (with blank line delimiters) and return a list of plates"
function getplates(grid)
    plates = []

    for i in 1:PLATEHEIGHT + 1:first(size(grid))
        push!(plates, grid[i : i + PLATEHEIGHT - 1, 1 : PLATEWIDTH])
    end
    plates
end

"calculate Z' factor screening statistic"
function zprime(positives, negatives)
    meanpos = mean(positives)
    meanneg = mean(negatives)
    sdpos = std(positives)
    sdneg = std(negatives)

    ((meanneg - 3sdneg) - (meanpos + 3sdpos)) / (meanneg - meanpos)
end

"calculate a normalised percentage"
function percentnorm(x, xmin, xmax) 
    100 * (x - xmin) / (xmax - xmin)
end

"coefficient of variation"
function variationcoefficient(values)
    std(values) / mean(values)
end

function normalise!(data, normtable)
    for row in eachrow(data)
        factor = mean(filter(x -> x.plate == row.plate, normtable).normvalue)
        row.meanvalue *= 1 / factor
    end
end

function stripid(str, ids)
    i = something(findfirst(' ', str), 0)
    if str[1:i-1] ∈ ids
        str[i+1:end]
    else
        str
    end
end

function platestotable(screens, libraries)
    records = []    
    for screen in screens
        library = libraries[findfirst(x -> x.libname == screen.libname, libraries)]
        for (plate, rep, wells) in zip(PLATEORDER, REPORDER, screen.plates)
            for i in eachindex(wells)
                push!(records,
                      (cellname = screen.cells,
                       libname = screen.libname,
                       plate = plate,
                       rep = rep,
                       drugname = stripid(library.layout[plate][i], library.library.drugid),
                       fluorescence = wells[i],
                       wellidx = i,
                       edgedist = edgedistancemanhattan(i)))
            end
        end
    end
    DataFrame(records)
end

function run()

    xlpath = "./drug_screen.xlsx"
    outdir = "./output/"

    xl = XLSX.readxlsx(xlpath)

    screens = @chain XLSX.sheetnames(xl) begin
        filter(sheet -> split(sheet, '_')[1] ∈ keys(CELLS), _)
        filter(sheet -> split(sheet, '_')[2] ∈ keys(SCREENS), _)
        map(sheet -> Screen(split(sheet, '_')..., getplates(xl[sheet][:])), _)
    end

    libraries = @chain XLSX.sheetnames(xl) begin
        filter(sheet -> split(sheet, '_')[2] == "layout", _)
        map(sheet -> Library(split(sheet, '_')[1], getplates(xl[sheet][:]), DataFrame()), _)
        map(lib -> Library(lib.libname, map(x -> strip.(x), lib.layout),
                           cleantable(DataFrame(XLSX.gettable(xl[string(lib.libname, "_library")])...))), _)
    end

    #libraryplots(libraries, outdir) # plot overview of drug libraries

    data = platestotable(screens, libraries)
    data.CellName = [CELLS[name] for name in data.cellname]

    platetable = @chain data begin
        groupby([:CellName, :drugname, :libname, :plate, :wellidx, :edgedist])
        combine(:fluorescence => mean => :mean,
                :fluorescence => median => :median,
                :fluorescence => minimum => :low,
                :fluorescence => maximum => :high,
                :fluorescence => variationcoefficient => :coeffvar)
    end

    mergedlib = foldr(vcat,
                      map(x -> begin
                              df = x.library
                              df[!, :libname] .= x.libname
                              df
                          end, libraries))

    results = innerjoin(mergedlib, platetable, on = [:drugname, :libname])
    # need to produce rep table, zprime, positive negative
    CSV.write(outdir * "drug_screen.tsv", results; delim = '\t')
    CSV.write(outdir * "drug_screen_replicates.tsv", data; delim = '\t')
end

# normalised percentage viability
# meannegatives = by(filter(x -> startswith(x.id, "negative"), platetable),
#                    [:plate], meanvalue = :meanvalue => mean)
# meannegatives.normvalue = map(x -> x / max(meannegatives.meanvalue...), meannegatives.meanvalue)
# normalise!(platetable, meannegatives)
# minviability = min(platetable.meanvalue...)
# meanofnegs = mean(filter(x -> startswith(x.id, "negative"), platetable).meanvalue)
# platetable.normpcviability = map(x -> percentnorm(x, minviability, meanofnegs), platetable.meanvalue)
# meanpos = mean(filter(x -> startswith(x.id, "pos"), platetable).normpcviability)
# println("Mean normalised viability of positive controls: ", meanpos)

    # manhattan distance
    # points with linear model overlaid
    
    # plate by plate results
    # individual points coloured by pathway

    # results by target/pathway
    # TBD
    
    # comparing across cell type
    # TBD
    
    # validation
    # TBD

# R"""
#     ggplot(reps, aes(x = eachplate, y = value)) +
#         geom_violin(aes(color = plate)) +
#         geom_jitter(data=filter(reps, startsWith(id, "negative")), color = "blue", width = 0.1) +
#         geom_jitter(data=filter(reps, startsWith(id, "positive")), color = "red", width = 0.1) +
#         geom_smooth(data=filter(reps, startsWith(id, "negative")), method="lm", color="red") +
#         ylab("CellTitre-Blue flourescence") +
#         facet_grid(rows = vars(cellline)) +
#         theme(axis.text.x = element_text(angle = 60, hjust = 1),
#               axis.title.x = element_blank(),
#               legend.position = "none")
#     """

# function libraryplots(libraries, outdir)
#     for library in libraries
#         df = library.library
        
#         # count instances of each pathway
#         counts = countmap(df.pathway)
#         pathcounts = DataFrame(pathway = collect(keys(counts)), n = collect(values(counts)))
        
#         # don't show unclassified drugs
#         knownpathways = filter(x -> x.pathway != "Others", pathcounts)
        
#         # call R to visualise pathways + targets
#         R"""
#         library(ggplot2); library(packcircles); library(Cairo)
#         circles <- function(data) { 
#             packing <- circleProgressiveLayout(data$n, sizetype='area')
#             data <- cbind(data, packing)
#             dat.gg <- circleLayoutVertices(packing, npoints=50)
        
#               plot <- ggplot() + 
#               geom_polygon(data = dat.gg, 
#                            aes(x, y, 
#                                    group = id, 
#                                fill=as.factor(id)), 
#                                colour = "black", 
#                                alpha = 0.6) +
#               geom_text(data = data, 
#                         aes(x, y, 
#                             label = pathway, 
#                             family = "FreeSans", 
#                             fontface = "bold")) +
#               scale_size_continuous(range = c(1,4)) +
#               theme_void() + 
#               theme(legend.position="none") +
#               coord_equal()
#               plot
#         }
#             ggsave(paste0($outdir, "library_pathways.pdf"), 
#                circles($knownpathways), 
#                device = cairo_pdf, 
#                width = 20, height = 20)
#         """
#     end
# end
