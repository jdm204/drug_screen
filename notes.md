# QC
## Plate effect
General increase in signal across plates (1 -> 5 and reps 1 -> 3) for
DG75s, likely due to longer time in CTB as reading the plates took
longer than CTB addition. Rajis were read alone (DG75s were read with
two other cell lines) and do not show the same effect.

# Hits
## Digoxin
Used as a cardiac medicine in humans, some evidence of suppression of
prostate cancer risk,, shown to kill tumours in mice but some evidence
that mouse cells are orders of magnitude less sensitive to this kind
of drug and so may not be selective.
## Carfilzomib
Proteasome inhibitor, potentially more selective than bortezomib.
