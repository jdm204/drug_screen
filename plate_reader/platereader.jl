using XLSX, DataFrames

const P384 = (16, 24) # rows, cols
const P96 = (8, 12)
const P24 = (4, 6)
const P6 = (2, 3)

const PR_OFFSET = (4, 3)

function extract(platedims, keyname, keypath, datapath; subset=(:, :), offset = (1, 1), addedgedist=true)
    key = xlarray(keypath, dims=platedims)
    data = xlarray(datapath, dims=platedims, offset=offset)

    coords = map(i -> getcoords(platedims, i), 1:prod(platedims))
    
    df = DataFrame(row = first.(coords), col = last.(coords), key = vec(key), value = vec(data))

    if addedgedist
        edgedists = map(x -> edgedist(platedims, x...), coords)
        df.edgedist = edgedists
    end
    
    filter!(x ->
        (first(subset) == (:) || x.row in first(subset)) &&
        (last(subset) == (:) || x.col in last(subset)), df)
    df
end

function xlarray(path; dims=(16, 24), sheet=1, offset=(1,1))
    XLSX.readxlsx(path)[sheet][xlrectanglestring(offset, dims)]
end

function xlrectanglestring(anchor, platedims)
    "$(xlidx(anchor...)):$(xlidx(anchor .+ platedims .- (1, 1)...))"
end

"return 1-based (row, column) coordinates from column-major index"
function getcoords(plate, i)
    col = div(i - 1, first(plate)) + 1
    row = i - ((col - 1) * first(plate))
    (row, col)
end

function edgedist(plate::Tuple{Int, Int}, row, col)
    min(row - 1,
        col - 1,
        first(plate) - row,
        last(plate) - col)
end

function xlidx(row, col)
    colletter = ('A':'Z')[col]
    "$colletter$row"
end
