#+title:  Optimising Vincristine and Methotrexate Doses for Persister Cells
#+author: Jamie D Matthews
#+date:  <2022-04-11 Mon>

* Intro

I want to drug screen persister cells in BL. First, I need to
establish reasonable concentrations of drugs such that we are
selecting a persister phenotype. For simplicity, I will start with
just two drugs - vincristine and methotrexate, a spindle poison and
antimetabolite respectively.

#+caption: Sorcha's Raji ED50s
| Drug         | ED50   |
|--------------+--------|
| Doxorubicin  | 150nM  |
| Methotrexate | 22nM   |
| Vincristine  | 1.58nM |

* Protocol

#+caption: Drug base stocks (-80℃)
| Drug         | w/v    | molar concentration |
|--------------+--------+---------------------|
| Vincristine  | 1mg/ml | 1mM                 |
| Methotrexate | 6mg/ml | 13.2mM              |

- seed cells at 0.5 million per ml (triplicate)
- add IC50 MTX + ED50 Vincristine
  - Vincristine: dilute 1 in 1000 to obtain 1μM stock, add this 1 in 634
  - Methotrexate: dilute 1 in 1000 to obtain 13.2μM stock, add this 1 in 600.
- count cells daily

* Data 📋

#+name: cellgrowth
#+caption: cellcount: millions of cells per ml. All cells treated with ED50 Vincristine + ED50 Methotrexate
| rep | cellcount | count | timestamp               |
|-----+-----------+-----+-------------------------|
|   1 |       0.5 |   1 | [2022-04-11 Mon 16:00] |
|   2 |       0.5 |   1 | [2022-04-11 Mon 16:00] |
|   3 |       0.5 |   1 | [2022-04-11 Mon 16:00] |
|   1 |      0.65 |   2 | [2022-04-12 Tue 09:25] |
|   2 |      1.07 |   2 | [2022-04-12 Tue 09:25] |
|   3 |      1.36 |   2 | [2022-04-12 Tue 09:25] |
|   1 |      1.14 |   3 | [2022-04-13 Wed 12:02] |
|   2 |      1.22 |   3 | [2022-04-13 Wed 12:03] |
|   3 |      1.38 |   3 | [2022-04-13 Wed 12:03] |
|   1 |      0.98 |   4 | [2022-04-14 Thu 15:00] |
|   2 |      1.26 |   4 | [2022-04-14 Thu 15:00] |
|   3 |      0.78 |   4 | [2022-04-14 Thu 15:00] |
|   1 |       0.8 |   5 | [2022-04-15 Fri 10:22] |
|   2 |      0.92 |   5 | [2022-04-15 Fri 10:22] |
|   3 |      1.16 |   5 | [2022-04-15 Fri 10:23] |
|   1 |      0.98 |   7 | [2022-04-17 Sun 15:58] |
|   2 |      1.02 |   7 | [2022-04-17 Sun 15:59] |
|   3 |      0.84 |   7 | [2022-04-17 Sun 16:02] |


#+name: cellgrowth2
#+caption: cellcount: millions of cells per ml. All cells treated with double ED50 Vincristine + double ED50 Methotrexate
| rep | cellcount | count | timestamp               |
|-----+-----------+-------+-------------------------|
|   1 |       0.5 |     1 | [2022-04-12 Tue 11:56] |
|   2 |       0.5 |     1 | [2022-04-12 Tue 11:56] |
|   3 |       0.5 |     1 | [2022-04-12 Tue 11:56] |
|   1 |      0.56 |     2 | [2022-04-13 Wed 13:24] |
|   2 |      0.84 |     2 | [2022-04-13 Wed 13:25] |
|   3 |      0.65 |     2 | [2022-04-13 Wed 13:25] |
|   1 |      0.64 |     3 | [2022-04-14 Thu 15:01] |
|   2 |      0.66 |     3 | [2022-04-14 Thu 15:01] |
|   3 |      0.44 |     3 | [2022-04-14 Thu 15:01] |
|   1 |      0.44 |     4 | [2022-04-15 Fri 10:23] |
|   2 |      0.46 |     4 | [2022-04-15 Fri 10:23] |
|   3 |      0.52 |     4 | [2022-04-15 Fri 10:23] |
|   1 |       0.7 |     6 | [2022-04-17 Sun 16:03] |
|   2 |      0.54 |     6 | [2022-04-17 Sun 16:05] |
|   3 |       0.4 |     6 | [2022-04-17 Sun 16:05] |
|   1 |      0.48 |     7 | [2022-04-18 Mon 09:35] |
|   2 |      0.36 |     7 | [2022-04-18 Mon 09:38] |
|   3 |       0.5 |     7 | [2022-04-18 Mon 09:38] |

#+name: cellgrowth3
| times | count | rep | cellcount | timestamp               |
|-------+-------+-----+-----------+-------------------------|
|     0 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     0 |     0 |   2 |       0.4 | [2022-04-17 Sun 17:06] |
|     0 |     0 |   3 |      0.42 | [2022-04-17 Sun 17:06] |
|     1 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     1 |     0 |   2 |       0.5 | [2022-04-17 Sun 17:06] |
|     1 |     0 |   3 |       0.5 | [2022-04-17 Sun 17:06] |
|     2 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     2 |     0 |   2 |      0.54 | [2022-04-17 Sun 17:06] |
|     2 |     0 |   3 |      0.45 | [2022-04-17 Sun 17:06] |
|     3 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     3 |     0 |   2 |      0.49 | [2022-04-17 Sun 17:06] |
|     3 |     0 |   3 |      0.44 | [2022-04-17 Sun 17:06] |
|     4 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     4 |     0 |   2 |      0.47 | [2022-04-17 Sun 17:06] |
|     4 |     0 |   3 |      0.51 | [2022-04-17 Sun 17:06] |
|     5 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     5 |     0 |   2 |      0.46 | [2022-04-17 Sun 17:06] |
|     5 |     0 |   3 |       0.5 | [2022-04-17 Sun 17:06] |
|     6 |     0 |   1 |       0.5 | [2022-04-17 Sun 17:06] |
|     6 |     0 |   2 |      0.51 | [2022-04-17 Sun 17:06] |
|     6 |     0 |   3 |      0.48 | [2022-04-17 Sun 17:06] |
|     0 |     1 |   1 |       0.7 | [2022-04-18 Mon 09:20] |
|     0 |     1 |   2 |      0.69 | [2022-04-18 Mon 09:20] |
|     0 |     1 |   3 |       0.7 | [2022-04-18 Mon 09:20] |
|     1 |     1 |   1 |      0.72 | [2022-04-18 Mon 09:20] |
|     1 |     1 |   2 |      0.67 | [2022-04-18 Mon 09:20] |
|     1 |     1 |   3 |      0.73 | [2022-04-18 Mon 09:20] |
|     2 |     1 |   1 |      0.56 | [2022-04-18 Mon 09:24] |
|     2 |     1 |   2 |      0.53 | [2022-04-18 Mon 09:24] |
|     2 |     1 |   3 |      0.57 | [2022-04-18 Mon 09:24] |
|     3 |     1 |   1 |      0.56 | [2022-04-18 Mon 09:25] |
|     3 |     1 |   2 |      0.65 | [2022-04-18 Mon 09:25] |
|     3 |     1 |   3 |      0.51 | [2022-04-18 Mon 09:25] |
|     4 |     1 |   1 |      0.54 | [2022-04-18 Mon 09:28] |
|     4 |     1 |   2 |       0.6 | [2022-04-18 Mon 09:28] |
|     4 |     1 |   3 |      0.56 | [2022-04-18 Mon 09:28] |
|     5 |     1 |   1 |      0.52 | [2022-04-18 Mon 09:29] |
|     5 |     1 |   2 |      0.53 | [2022-04-18 Mon 09:29] |
|     5 |     1 |   3 |      0.56 | [2022-04-18 Mon 09:29] |
|     6 |     1 |   1 |      0.36 | [2022-04-18 Mon 09:31] |
|     6 |     1 |   2 |      0.39 | [2022-04-18 Mon 09:31] |
|     6 |     1 |   3 |      0.37 | [2022-04-18 Mon 09:31] |
|     0 |     2 |   1 |       0.9 | [2022-04-19 Tue 09:14] |
|     0 |     2 |   2 |      1.01 | [2022-04-19 Tue 09:14] |
|     0 |     2 |   3 |      0.79 | [2022-04-19 Tue 09:14] |
|     1 |     2 |   1 |      0.58 | [2022-04-19 Tue 09:15] |
|     1 |     2 |   2 |       0.6 | [2022-04-19 Tue 09:15] |
|     1 |     2 |   3 |      0.57 | [2022-04-19 Tue 09:15] |
|     2 |     2 |   1 |      0.32 | [2022-04-19 Tue 09:19] |
|     2 |     2 |   2 |      0.35 | [2022-04-19 Tue 09:19] |
|     2 |     2 |   3 |      0.33 | [2022-04-19 Tue 09:19] |
|     3 |     2 |   1 |      0.28 | [2022-04-19 Tue 09:19] |
|     3 |     2 |   2 |      0.28 | [2022-04-19 Tue 09:19] |
|     3 |     2 |   3 |      0.29 | [2022-04-19 Tue 09:19] |
|     4 |     2 |   1 |      0.26 | [2022-04-19 Tue 09:23] |
|     4 |     2 |   2 |       0.3 | [2022-04-19 Tue 09:23] |
|     4 |     2 |   3 |      0.21 | [2022-04-19 Tue 09:23] |
|     5 |     2 |   1 |      0.24 | [2022-04-19 Tue 09:26] |
|     5 |     2 |   2 |      0.25 | [2022-04-19 Tue 09:26] |
|     5 |     2 |   3 |      0.24 | [2022-04-19 Tue 09:26] |
|     6 |     2 |   1 |      0.22 | [2022-04-19 Tue 09:31] |
|     6 |     2 |   2 |      0.22 | [2022-04-19 Tue 09:31] |
|     6 |     2 |   3 |       0.2 | [2022-04-19 Tue 09:31] |
|     0 |     3 |   1 |      1.84 | [2022-04-20 Wed 08:52] |
|     0 |     3 |   2 |      1.77 | [2022-04-20 Wed 08:52] |
|     0 |     3 |   3 |       1.7 | [2022-04-20 Wed 08:52] |
|     1 |     3 |   1 |      0.82 | [2022-04-20 Wed 08:52] |
|     1 |     3 |   2 |      0.83 | [2022-04-20 Wed 08:52] |
|     1 |     3 |   3 |       0.9 | [2022-04-20 Wed 08:52] |
|     2 |     3 |   1 |      0.66 | [2022-04-20 Wed 08:58] |
|     2 |     3 |   2 |       0.7 | [2022-04-20 Wed 08:58] |
|     2 |     3 |   3 |      0.66 | [2022-04-20 Wed 08:58] |
|     3 |     3 |   1 |       0.5 | [2022-04-20 Wed 08:59] |
|     3 |     3 |   2 |      0.45 | [2022-04-20 Wed 08:59] |
|     3 |     3 |   3 |      0.48 | [2022-04-20 Wed 08:59] |
|     4 |     3 |   1 |      0.36 | [2022-04-20 Wed 09:02] |
|     4 |     3 |   2 |      0.35 | [2022-04-20 Wed 09:02] |
|     4 |     3 |   3 |      0.36 | [2022-04-20 Wed 09:02] |
|     5 |     3 |   1 |      0.32 | [2022-04-20 Wed 09:05] |
|     5 |     3 |   2 |      0.31 | [2022-04-20 Wed 09:05] |
|     5 |     3 |   3 |       0.3 | [2022-04-20 Wed 09:05] |
|     6 |     3 |   1 |      0.28 | [2022-04-20 Wed 09:06] |
|     6 |     3 |   2 |      0.27 | [2022-04-20 Wed 09:06] |
|     6 |     3 |   3 |      0.29 | [2022-04-20 Wed 09:06] |
|     0 |     4 |   1 |      3.38 | [2022-04-21 Thu 08:44] |
|     0 |     4 |   2 |      2.88 | [2022-04-21 Thu 08:45] |
|     0 |     4 |   3 |      2.84 | [2022-04-21 Thu 08:45] |
|     1 |     4 |   1 |      1.02 | [2022-04-21 Thu 08:46] |
|     1 |     4 |   2 |      0.72 | [2022-04-21 Thu 08:47] |
|     1 |     4 |   3 |      0.88 | [2022-04-21 Thu 08:47] |
|     2 |     4 |   1 |       0.6 | [2022-04-21 Thu 08:51] |
|     2 |     4 |   2 |      0.66 | [2022-04-21 Thu 08:53] |
|     2 |     4 |   3 |      0.58 | [2022-04-21 Thu 08:53] |
|     3 |     4 |   1 |      0.22 | [2022-04-21 Thu 08:54] |
|     3 |     4 |   2 |      0.26 | [2022-04-21 Thu 08:54] |
|     3 |     4 |   3 |      0.18 | [2022-04-21 Thu 08:55] |
|     4 |     4 |   1 |       0.3 | [2022-04-21 Thu 08:58] |
|     4 |     4 |   2 |      0.48 | [2022-04-21 Thu 08:59] |
|     4 |     4 |   3 |       0.4 | [2022-04-21 Thu 08:59] |
|     5 |     4 |   1 |      0.42 | [2022-04-21 Thu 09:00] |
|     5 |     4 |   2 |       0.3 | [2022-04-21 Thu 09:00] |
|     5 |     4 |   3 |       0.4 | [2022-04-21 Thu 09:01] |
|     6 |     4 |   1 |      0.48 | [2022-04-21 Thu 09:04] |
|     6 |     4 |   2 |       0.6 | [2022-04-21 Thu 09:05] |
|     6 |     4 |   3 |      0.46 | [2022-04-21 Thu 09:06] |


* Plots 📈

Lubridate for datetime parsing:

#+begin_src R :session :results none
  library(tidyverse)
  library(lubridate)
#+end_src

Plot mean and SD of cell counts per ml for the ED50 case:

#+begin_src R :session :var df=cellgrowth :results graphics file :file vinc_meth_growth.svg :exports both
  df %>%
      mutate(dtm = parse_date_time(timestamp, '[%y/%m/%d %a %H:%M]'),
	     seconds = dtm - dtm[1],
             hours = seconds / 60^2) %>%
      select(-timestamp) %>%
      group_by(count) %>%
      summarise(hours = mean(hours),
                meancells = mean(cellcount),
                sdcells = sd(cellcount)) %>%
      ggplot() +
      geom_line(aes(hours, meancells)) +
      geom_ribbon(aes(x = hours,
                      ymin = meancells - sdcells,
                      ymax = meancells + sdcells),
                  alpha=0.1,
                  fill="blue") +
      theme_minimal() +
      labs(x = "time (hours)",
           y = "million cells per ml (mean of 3 flasks)")
#+end_src

#+RESULTS:
[[file:vinc_meth_growth.svg]]

Plot mean and SD of cell counts per ml for the double ED50 case:

#+begin_src R :session :var df=cellgrowth2 :results graphics file :file vinc_meth_growth2.svg :exports both
  df %>%
      mutate(dtm = parse_date_time(timestamp, '[%y/%m/%d %a %H:%M]'),
	     seconds = dtm - dtm[1],
             hours = seconds / 60^2) %>%
      select(-timestamp) %>%
      group_by(count) %>%
      summarise(hours = mean(hours),
                meancells = mean(cellcount),
                sdcells = sd(cellcount)) %>%
      ggplot() +
      geom_line(aes(hours, meancells)) +
      geom_ribbon(aes(x = hours,
                      ymin = meancells - sdcells,
                      ymax = meancells + sdcells),
                  alpha=0.1,
                  fill="blue") +
      theme_minimal() +
      labs(x = "time (hours)",
           y = "million cells per ml (mean of 3 flasks)")
#+end_src

#+RESULTS:
[[file:vinc_meth_growth2.svg]]



	     
#+begin_src R :session :var df=cellgrowth3 :results graphics file :file vinc_meth_growth_range.svg :exports both
	  df %>%
	      mutate(dtm = parse_date_time(timestamp, '[%y/%m/%d %a %H:%M]'),
		     seconds = dtm - dtm[1],
		     hours = seconds / 60^2,
		     times = factor(times, levels=c(0:6))) %>%
	      select(-timestamp) %>%
	      group_by(count, times) %>%
	      summarise(hours = mean(hours),
			meancells = mean(cellcount),
			sdcells = sd(cellcount)) %>%
	      ggplot(aes(hours, meancells, group=times, color=times, fill=times)) +
	      geom_line() +
	      geom_ribbon(aes(x = hours,
			      ymin = meancells - sdcells,
			      ymax = meancells + sdcells),
			  alpha=0.1,
			  color=NA) +
	      theme_minimal() +
	      labs(x = "time (hours)",
		   y = "million cells per ml (+/- measurement SD)",
		   color = "times\nSorcha's ED50",
		   fill = "times\nSorcha's ED50")
#+end_src

#+RESULTS:
[[file:vinc_meth_growth_range.svg]]

