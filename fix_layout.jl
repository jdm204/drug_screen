function swap!(grid, pt1, pt2)
    first = grid[pt1[1], pt1[2]]
    second = grid[pt2[1], pt2[2]]
    grid[pt1[1], pt1[2]] = second
    grid[pt2[1], pt2[2]] = first
end

function swap_points!(grid, pts1, pts2)
    for i in eachindex(pts1)
        swap!(grid, pts1[i], pts2[i])
    end
end

function fix_Raji_DG75_plate!(grid)
    swap_points!(grid,
                 Iterators.product(4:13, 23),
                 Iterators.product(4:13, 24))
end

